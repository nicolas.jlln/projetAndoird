package com.example.nikij.programme_projet;


import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;


public class ModeRec extends AppCompatActivity {

    private CustomButton b1;
    private CustomButton b2;
    private CustomButton b3;
    private CustomButton b4;
    private CustomButton b5;
    private CustomButton b6;
    private CustomButton b7;
    private CustomButton b8;
    private CustomButton b9;
    private CustomButton[] buttons = { null, null, null, null, null, null, null, null, null };


    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private static String mFileName = null;

    private MediaRecorder mRecorder = null;
    private MediaPlayer   mPlayer = null;

    // Requesting permission to RECORD_AUDIO
    private boolean permissionToRecordAccepted = false;
    private String [] permissions = {android.Manifest.permission.RECORD_AUDIO};
    private static final String LOG_TAG = "AudioRecord";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_rec);

        Bundle bundle = getIntent().getExtras();
        Log.w("ATTENTION ", "Au moins, on est arrivé à obtenir l'extra.");


        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/audiorecord.3gp";

        mPlayer = new MediaPlayer();

        final boolean[] mStartRecording = {true};

        final boolean[] mStartPlaying = {true};
        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);

        final Button mRecButton = findViewById(R.id.button_rec);
        final Button mPlayButton = findViewById(R.id.button_play);

        mRecButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                onRecord(mStartRecording[0]);
                if (mStartRecording[0]) {
                    mRecButton.setText("Stop recording");
                } else {
                    mRecButton.setText("Start recording");
                }
                mStartRecording[0] = !mStartRecording[0];
            }
        });

        mPlayButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onPlay(mStartPlaying[0]);
                if (mStartPlaying[0]) {
                    mPlayButton.setText(R.string.stopPlaying);
                } else {
                    mPlayButton.setText(R.string.startPlaying);
                }
                mStartPlaying[0] = !mStartPlaying[0];
            }
        });

        this.b1 = new CustomButton((Button)findViewById(R.id.REC_button1), ModeRec.this);
        this.b2 = new CustomButton((Button)findViewById(R.id.REC_button2), ModeRec.this);
        this.b3 = new CustomButton((Button)findViewById(R.id.REC_button3), ModeRec.this);
        this.b4 = new CustomButton((Button)findViewById(R.id.REC_button4), ModeRec.this);
        this.b5 = new CustomButton((Button)findViewById(R.id.REC_button5), ModeRec.this);
        this.b6 = new CustomButton((Button)findViewById(R.id.REC_button6), ModeRec.this);
        this.b7 = new CustomButton((Button)findViewById(R.id.REC_button7), ModeRec.this);
        this.b8 = new CustomButton((Button)findViewById(R.id.REC_button8), ModeRec.this);
        this.b9 = new CustomButton((Button)findViewById(R.id.REC_button9), ModeRec.this);

        this.buttons[0] = this.b1;
        this.buttons[1] = this.b2;
        this.buttons[2] = this.b3;
        this.buttons[3] = this.b4;
        this.buttons[4] = this.b5;
        this.buttons[5] = this.b6;
        this.buttons[6] = this.b7;
        this.buttons[7] = this.b8;
        this.buttons[8] = this.b9;

        int i = 1;
        for(final CustomButton b: this.buttons){
            b.setName(bundle.getString("name"+String.valueOf(i)));
            b.setColor(bundle.getString("color"+String.valueOf(i)));
            b.setSound(bundle.getString("sound"+String.valueOf(i)));

            b.getButton().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        if(b.getSound() != null) {
                            //mPlayer.release();
                            mPlayer = MediaPlayer.create(ModeRec.this, Uri.parse(b.getSound()));
                            mPlayer.setLooping(false);
                            mPlayer.start();
                        }
                    } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        mPlayer.release();
                    }else
                    {
                        Toast.makeText(ModeRec.this, "Aucun son", Toast.LENGTH_SHORT).show();
                    }
                    return false;
                }

            });
            b.activate();
            i++;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted ) finish();

    }

    private void onRecord(boolean start) {
        if (start) {
            startRecording();
        } else {
            stopRecording();
        }
    }

    private void onPlay(boolean start) {
        if (start) {
            startPlaying();
        } else {
            stopPlaying();
        }
    }

    private void startPlaying() {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        mRecorder.start();
    }

    private void stopRecording() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }

        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }
}
