package com.example.nikij.programme_projet;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.LogPrinter;
import android.view.Display;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Menu_app extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // Menu Utils
    private Button bt_creaMode;
    private Button bt_RECmode;
    private Button bt_reprendre;
    private FloatingActionButton fbt_play;
    private Button bt_connect;
    private Button bt_revoke;
    private Button nav_sounds;

    // Sub views :
    private View app_bar_view;
    private View nav_header_view;

    // Google :
    private GoogleSignInClient mGoogleSignInClient;

    private ImageView iv_googleProfile;
    private TextView tv_googleName;
    private TextView tv_googleEmail;

    private static int RC_SIGN_IN = 100;

    //mediaPlayer
    private MediaPlayer mPlayer= null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_app);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mPlayer = new MediaPlayer();

        // Sub views :
        this.app_bar_view = findViewById(R.id.app_bar_menu);
        this.nav_header_view = navigationView.inflateHeaderView(R.layout.nav_header_menu_app);

        this.nav_sounds = nav_header_view.findViewById(R.id.nav_sounds);


        this.bt_creaMode = (Button) findViewById(R.id.menu_button_mode_crea);
        this.bt_RECmode = (Button) findViewById(R.id.menu_button_mode_REC);
        this.bt_reprendre = (Button) findViewById(R.id.menu_button_reprendre);
        this.fbt_play = (FloatingActionButton) findViewById(R.id.menu_floatingbutton_play);


        // Google :
        this.iv_googleProfile = (ImageView) nav_header_view.findViewById(R.id.menu_imageView_googleSignIn);
        this.tv_googleName = (TextView) nav_header_view.findViewById(R.id.menu_textView_googleSignInName);
        this.tv_googleEmail = (TextView) nav_header_view.findViewById(R.id.menu_textView_googleSignInEmail);
        this.bt_connect = (Button) nav_header_view.findViewById(R.id.menu_button_connectGoogleAccount);
        this.bt_revoke = (Button) nav_header_view.findViewById(R.id.menu_button_revokeGoogleAccount);


        bt_creaMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Menu_app.this, CreaMode.class);
                startActivity(intent);
            }
        });

        bt_RECmode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Menu_app.this, ModeSoundRecording.class);
                startActivity(intent);
            }
        });

        bt_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });
        bt_revoke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                revokeAccess();
            }
        });
        updateUI(null);

        final boolean[] i = {false};

        fbt_play.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(i[0]) {
                    playSound(R.raw.coin);
                    i[0] = false;
                }else
                {
                    playSound(R.raw.up);
                    i[0] = true;
                }
            }

        });

        updateUI(null);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app, menu);
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_sounds) {
            Intent intent = new Intent(Menu_app.this, sons.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onStart() {
        super.onStart();
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }

    private void signIn() {
        // Pour API Google !!
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Mon WARNING", "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(@Nullable GoogleSignInAccount acc){
        if(acc != null) {
            try {
                bt_connect.setVisibility(View.GONE);
                bt_revoke.setVisibility(View.VISIBLE);

                tv_googleEmail.setText(acc.getEmail());
                tv_googleName.setText(acc.getDisplayName());
                String url = acc.getPhotoUrl().toString();
                iv_googleProfile.setImageBitmap(getBitmapFromURL(url));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            bt_connect.setVisibility(View.VISIBLE);
            bt_revoke.setVisibility(View.GONE);
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap btm = BitmapFactory.decodeStream(input);
            return btm;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    private void revokeAccess() {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUI(null);
                    }
                });
    }

    private void playSound(int resId) {
        mPlayer = MediaPlayer.create(this, resId);
        mPlayer.start();
    }


}
