package com.example.nikij.programme_projet;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class CreaMode extends AppCompatActivity {

    private Button recMode;
    private Button saveREC;

    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button button5;
    private Button button6;
    private Button button7;
    private Button button8;
    private Button button9;

    private String mURLString;


    private CustomButton[] buttons = { null, null, null, null, null, null, null, null, null };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crea_mode);

        this.recMode = (Button) findViewById(R.id.modeCrea_button_passer_mode_REC);
        this.saveREC = (Button) findViewById(R.id.modeCrea_button_save_REC);

        this.button1 = (Button) findViewById(R.id.crea_button1);
        this.button2 = (Button) findViewById(R.id.crea_button2);
        this.button3 = (Button) findViewById(R.id.crea_button3);
        this.button4 = (Button) findViewById(R.id.crea_button4);
        this.button5 = (Button) findViewById(R.id.crea_button5);
        this.button6 = (Button) findViewById(R.id.crea_button6);
        this.button7 = (Button) findViewById(R.id.crea_button7);
        this.button8 = (Button) findViewById(R.id.crea_button8);
        this.button9 = (Button) findViewById(R.id.crea_button9);

        final Context creaModeContext = CreaMode.this;

        final Button ok = (Button) findViewById(R.id.dialog_saveREC_button_ok);

        this.buttons[0] = new CustomButton(button1, creaModeContext);
        this.buttons[1] = new CustomButton(button2, creaModeContext);
        this.buttons[2] = new CustomButton(button3, creaModeContext);
        this.buttons[3] = new CustomButton(button4, creaModeContext);
        this.buttons[4] = new CustomButton(button5, creaModeContext);
        this.buttons[5] = new CustomButton(button6, creaModeContext);
        this.buttons[6] = new CustomButton(button7, creaModeContext);
        this.buttons[7] = new CustomButton(button8, creaModeContext);
        this.buttons[8] = new CustomButton(button9, creaModeContext);

        int i = 0; // Counter for counting which CustomButton we are configuring.

        for(final CustomButton b: this.buttons){
            final int buttonPosition = i;
            b.getButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder mbuilder = new AlertDialog.Builder(CreaMode.this);
                    View mView = getLayoutInflater().inflate(R.layout.dialog_configbtn, null);
                    final EditText mName = (EditText) mView.findViewById(R.id.dialog_button_name);
                    final Spinner mSlectedColor = (Spinner) mView.findViewById(R.id.dialog_spinner_colors);
                    final TextView mSelectedFile = (TextView) mView.findViewById(R.id.dialog_selected_sound_name);
                    final Button mFileSelecterButton = (Button) mView.findViewById(R.id.dialog_button_select_sound);
                    Button ok = (Button) mView.findViewById(R.id.dialog_button_ok);

                    mFileSelecterButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(CreaMode.this, ExplorateurActivity.class);
                            intent.putExtra("butPos", buttonPosition);
                            startActivityForResult(intent, 1);
                        }
                    });

                    mbuilder.setView(mView);
                    final AlertDialog dialog = mbuilder.create();
                    dialog.show();

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mSelectedFile.setText(b.getSound());
                            if(!mName.getText().toString().equals("Nom")
                                    && !(mSlectedColor.getSelectedItem()==null)
                                    && !b.getSound().equals("")){//!mSelectedFile.getText().toString().equals("")) {
                                b.setName(mName.getText().toString());
                                b.setColor(mSlectedColor.getSelectedItem().toString());

                                Toast.makeText(CreaMode.this, R.string.button_configuration_success, Toast.LENGTH_SHORT).show();
                                if(dialog.isShowing()){
                                    dialog.hide();
                                }
                            }else{
                                Toast.makeText(CreaMode.this, R.string.button_configuration_fail, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            });
            i = i + 1; //We switch to another CustomButton
        }

        this.recMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreaMode.this, ModeRec.class);
                int i = 1;
                for(CustomButton c: buttons){
                    intent.putExtra(String.valueOf("name"+String.valueOf(i)), c.getName());
                    intent.putExtra(String.valueOf("color"+String.valueOf(i)), c.getColor());
                    intent.putExtra(String.valueOf("sound"+String.valueOf(i)), c.getSound());
                    i++;
                }
                startActivity(intent);
            }
        });

        this.saveREC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final RecSave recSave = new RecSave();
                String[] names = { null, null, null, null, null, null, null, null, null };
                String[] colors = { null, null, null, null, null, null, null, null, null };
                String[] urlSounds = { null, null, null, null, null, null, null, null, null };

                int i = 0;
                for (CustomButton c : buttons) {
                    names[i] = c.getName();
                    colors[i] = c.getColor();
                    urlSounds[i] = c.getSound();
                    i++;
                }

                recSave.setNames(names);
                recSave.setColors(colors);
                recSave.setUrls(urlSounds);

                final AlertDialog.Builder SaveRecBuilder = new AlertDialog.Builder(CreaMode.this);
                View mView = getLayoutInflater().inflate(R.layout.dialog_save_rec, null);
                final EditText mName = (EditText) mView.findViewById(R.id.dialog_saveREC_button_name);
                Button ok = (Button) mView.findViewById(R.id.dialog_saveREC_button_ok);


                SaveRecBuilder.setView(mView);
                final AlertDialog dialog = SaveRecBuilder.create();
                dialog.show();

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // j'ai modif ici y'a un probleme pour appuyer sur le bouton OK
                        //if(true){
                        if(!mName.getText().toString().equals("Nom")){
                            recSave.setSaveName(mName.getText().toString());
                            if(recSave.saveREC(creaModeContext)) {
                                if (dialog.isShowing()) {
                                    dialog.hide();
                                }
                                Toast.makeText(CreaMode.this, "Sauvegardé avec succes !", Toast.LENGTH_SHORT).show();
                            }
                            Toast.makeText(CreaMode.this, "Probleme rencontré lors de la sauvegarde", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(CreaMode.this, "Veuillez saisir un nom svp.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case 1:
                Random rand = new Random();
                int randPos = rand.nextInt(9) + 1;
                int Position = data.getIntExtra("butPos", randPos);
                this.mURLString = data.getStringExtra("result");
                this.buttons[Position].setSound(data.getStringExtra("result"));
                break;
        }
    }
}