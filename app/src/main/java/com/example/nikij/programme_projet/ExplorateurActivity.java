package com.example.nikij.programme_projet;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ExplorateurActivity extends ListActivity implements OnSharedPreferenceChangeListener {

    private TextView mEmpty = null;

    private ListView mList = null;

    private FileAdapter mAdapter = null;

    private File mCurrentFile = null;

    private int mColor = 0;

    private boolean mCountdown = false;

    private SharedPreferences mPrefs = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explorateur);

        Bundle bundle = getIntent().getExtras();
        final int butPos = bundle.getInt("butPos");

        mList = (ListView) getListView();

        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            mEmpty = (TextView) mList.getEmptyView();
            mEmpty.setText("Vous ne pouvez pas acceder aux fichiers");
        } else {
            registerForContextMenu(mList);

            mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

            mPrefs.registerOnSharedPreferenceChangeListener(this);

            mColor = mPrefs.getInt("repertoireColorPref", Color.RED);

            mCurrentFile = Environment.getExternalStorageDirectory();

            setTitle(mCurrentFile.getAbsolutePath());

            File[] fichiers = mCurrentFile.listFiles();

            ArrayList <File> liste = new ArrayList <File>();
            if(fichiers !=null)
                liste.addAll(Arrays.asList(fichiers));

            mAdapter = new FileAdapter(this, android.R.layout.simple_list_item_1, liste);

            mList.setAdapter(mAdapter);

            mAdapter.sort();

            mList.setOnItemClickListener(new OnItemClickListener() {

                // Que se passe-il en cas de cas de clic sur un �l�ment de la liste ?
                public void onItemClick(AdapterView <?> adapter, View view,
                                        int position, long id) {
                    File fichier = mAdapter.getItem(position);
                    // Si le fichier est un r�pertoire...
                    if (fichier.isDirectory())
                        // On change de r�pertoire courant
                        updateDirectory(fichier);
                    else {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result",fichier.getAbsolutePath());
                        returnIntent.putExtra("butPos", butPos);
                        setResult(1,returnIntent);
                        finish();

                    }

                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_explorateur, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_options:
                // Intent explicite
                Intent i = new Intent(this, ExploreurPreference.class);
                startActivity(i);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View vue,
                                    ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, vue, menuInfo);

        MenuInflater inflater = getMenuInflater();
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) menuInfo;


        File fichier = mAdapter.getItem(info.position);
        if (!fichier.isDirectory())
            inflater.inflate(R.menu.context_file, menu);
        else
            inflater.inflate(R.menu.context_dir, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        File fichier = mAdapter.getItem(info.position);
        switch (item.getItemId()) {
            case R.id.deleteItem:
                mAdapter.remove(fichier);
                fichier.delete();
                return true;

            case R.id.seeItem:
                seeItem(fichier);
                return true;
        }
        return super.onContextItemSelected(item);
    }

    /**
     * Utilis� pour visualiser un fichier
     *
     * @param pFile le fichier � visualiser
     */
    private void seeItem(File pFile) {

        Intent i = new Intent(Intent.ACTION_VIEW);

        // On d�termine son type MIME
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String ext = pFile.getName().substring(pFile.getName().indexOf(".") + 1).toLowerCase();
        String type = mime.getMimeTypeFromExtension(ext);

        // On ajoute les informations n�cessaires
        i.setDataAndType(Uri.fromFile(pFile), type);

        try {
            startActivity(i);
            // Et s'il n'y a pas d'activit� qui puisse g�rer ce type de fichier
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "Oups, vous n'avez pas d'application qui puisse lancer ce fichier", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    /**
     * Utilis� pour naviguer entre les r�pertoires
     *
     * @param pFile le nouveau r�pertoire dans lequel aller
     */

    public void updateDirectory(File pFile) {
        // On change le titre de l'activit�
        setTitle(pFile.getAbsolutePath());

        // L'utilisateur ne souhaite plus sortir de l'application
        mCountdown = false;

        // On change le repertoire actuel
        mCurrentFile = pFile;
        // On vide les r�pertoires actuels
        setEmpty();

        // On r�cup�re la liste des fichiers du nouveau r�pertoire
        File[] fichiers = mCurrentFile.listFiles();

        // Si le r�pertoire n'est pas vide...
        if (fichiers != null)
            // On les ajoute �  l'adaptateur
            for (File f : fichiers)
                mAdapter.add(f);
        // Et on trie l'adaptateur
        mAdapter.sort();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Si on a appuy� sur le retour arri�re
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // On prend le parent du r�pertoire courant
            File parent = mCurrentFile.getParentFile();
            // S'il y a effectivement un parent
            if (parent != null)
                updateDirectory(parent);
            else {
                // Sinon, si c'est la premi�re fois qu'on fait un retour arri�re
                if (mCountdown != true) {
                    // On indique � l'utilisateur qu'appuyer dessus une seconde fois le fera sortir
                    Toast.makeText(this, "Nous sommes d�j� � la racine ! Cliquez une seconde fois pour quitter", Toast.LENGTH_SHORT).show();
                    mCountdown = true;
                } else
                    // Si c'est la seconde fois on sort effectivement
                    finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * On enl�ve tous les �l�ments de la liste
     */

    public void setEmpty() {
        // Si l'adapteur n'est pas vide...
        if (!mAdapter.isEmpty())
            // Alors on le vide !
            mAdapter.clear();
    }

    /**
     * L'adaptateur sp�cifique � nos fichiers
     */

    private class FileAdapter extends ArrayAdapter <File> {
        /**
         * Permet de comparer deux fichiers
         */
        private class FileComparator implements Comparator <File> {

            public int compare(File lhs, File rhs) {
                // si lhs est un r�pertoire et pas l'autre, il est plus petit
                if (lhs.isDirectory() && rhs.isFile())
                    return -1;
                // dans le cas inverse, il est plus grand
                if (lhs.isFile() && rhs.isDirectory())
                    return 1;

                //Enfin on ordonne en fonction de l'ordre alphab�tique sans tenir compte de la casse
                return lhs.getName().compareToIgnoreCase(rhs.getName());
            }

        }

        public FileAdapter(Context context, int textViewResourceId,
                           List <File> objects) {
            super(context, textViewResourceId, objects);
            mInflater = LayoutInflater.from(context);
        }

        private LayoutInflater mInflater = null;

        /**
         * Construit la vue en fonction de l'item
         */
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView vue = null;

            if (convertView != null)
                vue = (TextView) convertView;
            else
                vue = (TextView) mInflater.inflate(android.R.layout.simple_list_item_1, null);
            File item = getItem(position);
            //Si c'est un r�pertoire, on choisit la couleur dans les pr�f�rences
            if (item.isDirectory())
                vue.setTextColor(mColor);
            else
                // Sinon c'est du noir
                vue.setTextColor(Color.BLACK);
            vue.setText(item.getName());
            return vue;
        }

        /**
         * Pour trier rapidement les �l�ments de l'adaptateur
         */
        public void sort() {
            super.sort(new FileComparator());
        }
    }

    /**
     * Se d�clenche d�s qu'une pr�f�rence a chang�
     */
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {
        mColor = sharedPreferences.getInt("repertoireColorPref", Color.BLACK);
        mAdapter.notifyDataSetInvalidated();
    }
}
